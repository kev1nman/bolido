<footer>
  <div class="info" id="contacto">
    <div class="container">

      <div class="row">
        <div class="col-md-4">
          <div class="item">
            <h5><span class="ion-ios-location"></span> Ubicación:</h5>
            <p>
              Av. Nueva Providencia 1945, Of. 812 <br>
              Metro Pedro de Valdivia, Providencia - Santiago.
            </p>
          </div>
          <div class="item">
              <h5><span class="ion-ios-clock"></span> Horarios:</h5>
              <p>
                  Lunes a Viernes: 8:30 – 21:00 <br>
                  Sábados: 9:00 a 14:00
              </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
              <h5><span class="ion-android-car"></span> ¿ Cómo llegar ?</h5>
              <p>
                  Puedes utilizar nuestra ubicación a través de Waze para llegar al destino.
              </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
              <h5>Contáctos:</h5>
              <div class="row">
                <div class="col-md-6">
                    <a href="tel:29798150"><span class="ion-ios-telephone"></span> 2 979 8150</a>
                </div>
                <div class="col-md-6">
                    <a href="mailto:email@email.com"><span class="ion-ios-email"></span> email@email.com</a>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer">
    <div class="container">
      <p class="text-muted">Place sticky footer content here.</p>
    </div>
  </div>
</footer>




<!-- REQUIRED JS SCRIPTS -->


<!-- jQuery 3 -->
<script src='vendor/jquery/jquery.min.js'></script>
<!-- Bootstrap 3.3.7 -->
<script src='vendor/bootstrap/js/bootstrap.min.js'></script>
<!-- Gallery -->
<script type="text/javascript" src="js/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="js/jquery.elastislide.js"></script>
		<script type="text/javascript" src="js/gallery.js"></script>

<!-- My-App -->
<script src='js/my-app.js' type='text/javascript'></script>


<script>
  $(document).ready(function () {
    $('input#input_text, textarea#textarea1').characterCounter();
  });
</script>


</body>

</html>