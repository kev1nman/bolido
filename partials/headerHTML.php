<!DOCTYPE html>
    <html lang="es">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bolido</title>

        

        <!-- Favicon -->
        <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'>
        <link rel='icon' href='img/favicon.ico' type='image/x-icon'>

        <!-- Iconos -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css">

        <!-- Slider -->
        <link href="css/demo.css" rel="stylesheet">
        <link href="css/style3.css" rel="stylesheet">
        <script type="text/javascript" src="js/modernizer.js"></script>

        <!-- Gallery -->
        <link rel="stylesheet" type="text/css" href="css/style-gallery.css" />
		<link rel="stylesheet" type="text/css" href="css/gallery.css" />
		<link rel="stylesheet" type="text/css" href="css/elastislide-gallery.css" />


        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="css/my-app.css" rel="stylesheet">

        <script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>

        

    </head>

    <body>