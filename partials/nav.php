<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">
                <img class="brand2 hide" alt="Brand" src="img/forma.png">
                <img class="brand" alt="Brand" src="img/forma.png"> BOLIDO
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class=""><a href="#">Equipamiento</a></li>
                <li><a href="./marcas.php">En venta</a></li>
                <li><a href="#">Contácto</a></li>
                <li><a href="#" class="ws"><i class="ion-social-whatsapp"></i> 2 979 8150</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>