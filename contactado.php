<?php include "partials/headerHTML.php"; ?>


<section class="contactado">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>
                    Nos contactaremos
                </h1>
                <h2>
                    a la brevedad con usted!
                </h2>
            </div>
        </div>
    </div>
</section>




<!-- REQUIRED JS SCRIPTS -->


<!-- jQuery 3 -->
<script src='vendor/jquery/jquery.min.js'></script>
<!-- Bootstrap 3.3.7 -->
<script src='vendor/bootstrap/js/bootstrap.min.js'></script>
<!-- materialize -->
<script src="js/materialize.js"></script>
<!-- My-App -->
<script src='js/my-app.js' type='text/javascript'></script>


<script>
  $(document).ready(function () {
    $('input#input_text, textarea#textarea1').characterCounter();
  });
</script>

<style>
    body {
        padding: 0;
        margin: 0;
    }
</style>



<!-- Optionally, you can add Slimscroll and FastClick plugins.
    Both of these plugins are recommended to enhance the
    user experience. -->
</body>

</html>