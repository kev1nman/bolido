<?php include "partials/headerHTML.php"; include "partials/nav.php"; ?>

<section class="header" id="porque">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 noP">
                <div class="slider" id="page">
                    <ul class="cb-slideshow">
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                    </ul>
                </div>





            </div>
        </div>
    </div>
</section>

<section id="detalles-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2>Titulo</h2>
            </div>
            <div class="col-md-8">
                <div id="rg-gallery" class="rg-gallery">
					<div class="rg-thumbs">
						<!-- Elastislide Carousel Thumbnail Viewer -->
						<div class="es-carousel-wrapper">
							<div class="es-nav">
								<span class="es-nav-prev">Previous</span>
								<span class="es-nav-next">Next</span>
							</div>
							<div class="es-carousel">
								<ul>
									<li><a href="#"><img src="img/fotos/1.jpeg" data-large="img/fotos/1.jpeg" alt="image01" data-description="From off a hill whose concave womb reworded" /></a></li>
									<li><a href="#"><img src="img/fotos/2.jpeg" data-large="img/fotos/2.jpeg" alt="image02" data-description="A plaintful story from a sistering vale" /></a></li>
									<li><a href="#"><img src="img/fotos/3.jpeg" data-large="img/fotos/3.jpeg" alt="image03" data-description="A plaintful story from a sistering vale" /></a></li>
                                    <li><a href="#"><img src="img/fotos/4.jpeg" data-large="img/fotos/4.jpeg" alt="image04" data-description="My spirits to attend this double voice accorded" /></a></li>
                                </ul>
							</div>
						</div>
						<!-- End Elastislide Carousel Thumbnail Viewer -->
					</div><!-- rg-thumbs -->
				</div><!-- rg-gallery -->

                <div class="ficha">
                    <h4>Ficha técnica</h4>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>Precio</th>
                                    <th>$7.190.000</th>
                                    <th>$7.190.000</th>
                                    <th>$7.190.000</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                                <tr>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                    <td>Año:</td>
                                    <td class="col">2017</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="col-md-4">

                <div class="table-responsive">
                    <table class="table fichaSimple">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Precio</th>
                                <th>$7.190.000</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="ion-calendar"></span> Año:</td>
                                <td>2017</td>
                            </tr>
                            <tr>
                                <td><span class="ion-speedometer"></span> Kilometraje:</td>
                                <td>11444</td>
                            </tr>
                            <tr>
                                <td><span class="ion-network"></span> Transmisióon:</td>
                                <td>Manual</td>
                            </tr>
                            <tr>
                                <td><span class="ion-flash"></span> Combustible:</td>
                                <td>Gasolina</td>
                            </tr>
                        </tbody>
                    </table>
                </div>


                <div class="equipamiento">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#marca1" aria-controls="marca1" role="tab" data-toggle="tab"><i class="ion-ios-calculator"></i> Cotiza aqui</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="marca1">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><i class="ion-ios-calculator"></i> Cotiza aqui</h4>
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">File input</label>
                                            <input type="file" id="exampleInputFile">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<div id="detalles-page">
    <section class="sect2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="vehiculos">

                        <div class="row">
                            <div class="col-md-12">
                                <h4>Tambien te puede interesar</h4>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <button class="btn btn-info pull-left">Ver detalles</button>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <button class="btn btn-info pull-left">Ver detalles</button>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <button class="btn btn-info pull-left">Ver detalles</button>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>

                            

                        </div>

                        <div class="row">
                                <div class="col-md-12">
                                        <div class="marcas">
                                            <div class="row">
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

</div>












<?php  include "modals.php"; include "partials/footer.php"; ?>