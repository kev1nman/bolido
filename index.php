<?php include "partials/headerHTML.php"; include "partials/nav.php"; ?>

<section class="header" id="porque">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 noP">
                <div class="slider" id="page">
                    <ul class="cb-slideshow">
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                    </ul>
                </div>





                <div class="equipamiento">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#marca1" aria-controls="marca1" role="tab" data-toggle="tab"><i class="ion-ios-calculator"></i> Cotiza aqui</a></li>
                        <li role="presentation"><a href="#marca2" aria-controls="marca2" role="tab" data-toggle="tab"><i class="ion-speedometer"></i> Repuestos y accesorios</a></li>
                        <li role="presentation"><a href="#marca3" aria-controls="marca3" role="tab" data-toggle="tab"><i class="ion-model-s"></i> Equipa tu vehiculo</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="marca1">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><i class="ion-ios-calculator"></i> Cotiza aqui</h4>
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">File input</label>
                                            <input type="file" id="exampleInputFile">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="marca2">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><i class="ion-speedometer"></i> Repuestos y accesorios</h4>
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">File input</label>
                                            <input type="file" id="exampleInputFile">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="marca3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><i class="ion-model-s"></i> Equipa tu vehiculo</h4>
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">File input</label>
                                            <input type="file" id="exampleInputFile">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Enviar</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>



<section class="sect2" id="menu2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="marcas">
                    <div class="row">
                        <div class="col-xs-3 col-sm-2">
                            <img class="img-responsive" src="img/marca1.png" alt="">
                        </div>
                        <div class="col-xs-3 col-sm-2">
                            <img class="img-responsive" src="img/marca1.png" alt="">
                        </div>
                        <div class="col-xs-3 col-sm-2">
                            <img class="img-responsive" src="img/marca1.png" alt="">
                        </div>
                        <div class="col-xs-3 col-sm-2">
                            <img class="img-responsive" src="img/marca1.png" alt="">
                        </div>
                        <div class="col-xs-3 col-sm-2">
                            <img class="img-responsive" src="img/marca1.png" alt="">
                        </div>
                        <div class="col-xs-3 col-sm-2">
                            <img class="img-responsive" src="img/marca1.png" alt="">
                        </div>
                        <div class="col-xs-3 col-sm-2">
                            <img class="img-responsive" src="img/marca1.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="vehiculos">
                    <h2>Destacados</h2>

                    <div class="row">
                        <div class="col-sm-6 col-md-4 text-center">
                            <div class="box">
                                <div class="img">
                                    <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                    <span class="estatus">Recién llegado</span>
                                </div>
                                <div class="info">
                                    <h4>Titulo</h4>
                                    <span class="precio pull-right">$12.990.00</span>
                                    <div class="specs">
                                        <div class="col-xs-4 border">
                                            2013
                                        </div>
                                        <div class="col-xs-4 border">
                                            Gasolina
                                        </div>
                                        <div class="col-xs-4">
                                            Automático
                                        </div>
                                        <div class="col-xs-12">
                                            <br>
                                            <span class="ion-model-s"></span> 54000 km
                                        </div>
                                    </div>
                                    <a href="marcas.php" class="btn btn-primary">Ver mas</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 text-center">
                            <div class="box">
                                <div class="img">
                                    <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                    <span class="estatus">Recién llegado</span>
                                </div>
                                <div class="info">
                                    <h4>Titulo</h4>
                                    <span class="precio pull-right">$12.990.00</span>
                                    <div class="specs">
                                        <div class="col-xs-4 border">
                                            2013
                                        </div>
                                        <div class="col-xs-4 border">
                                            Gasolina
                                        </div>
                                        <div class="col-xs-4">
                                            Automático
                                        </div>
                                        <div class="col-xs-12">
                                            <br>
                                            <span class="ion-model-s"></span> 54000 km
                                        </div>
                                    </div>
                                    <a href="marcas.php" class="btn btn-primary">Ver mas</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 text-center">
                            <div class="box">
                                <div class="img">
                                    <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                    <span class="estatus">Recién llegado</span>
                                </div>
                                <div class="info">
                                    <h4>Titulo</h4>
                                    <span class="precio pull-right">$12.990.00</span>
                                    <div class="specs">
                                        <div class="col-xs-4 border">
                                            2013
                                        </div>
                                        <div class="col-xs-4 border">
                                            Gasolina
                                        </div>
                                        <div class="col-xs-4">
                                            Automático
                                        </div>
                                        <div class="col-xs-12">
                                            <br>
                                            <span class="ion-model-s"></span> 54000 km
                                        </div>
                                    </div>
                                    <a href="marcas.php" class="btn btn-primary">Ver mas</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<a href="#">
    <section class="banner">
        <div class="container-fluid">

        </div>
    </section>
</a>


<section class="sect3">
    <div class="container-fluid">
        <div class="row">
               <div class="col-md-12">
                    <h2>Repuestos y accesorios</h2>
               </div>
            
            <div class="col-md-6">
                <div class="box">
                    <img class="img" src="./img/fotos/1.jpeg">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                        
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                    <div class="box">
                        <img class="img" src="./img/fotos/1.jpeg">
                        <h4>Lorem ipsum dolor sit amet</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                            
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                        <div class="box">
                            <img class="img" src="./img/fotos/1.jpeg">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                                
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                            <div class="box">
                                <img class="img" src="./img/fotos/1.jpeg">
                                <h4>Lorem ipsum dolor sit amet</h4>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                                    
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quos repudiandae distinctio saepe vel est quae fugiat voluptatem, culpa, quis aspernatur possimus voluptatibus dignissimos sit dicta ipsum iure quia itaque?
                                </p>
                            </div>
                        </div>
        </div>
    </div>
</section>











<?php  include "modals.php"; include "partials/footer.php"; ?>