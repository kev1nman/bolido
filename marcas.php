<?php include "partials/headerHTML.php"; include "partials/nav.php"; ?>

<section class="header" id="porque">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 noP">
                <div class="slider" id="page">
                    <ul class="cb-slideshow">
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>re·lax·a·tion</h3>
                            </div>
                        </li>
                        <li><span></span>
                            <div>
                                <h3>qui·e·tude</h3>
                            </div>
                        </li>
                    </ul>
                </div>





            </div>
        </div>
    </div>
</section>


<div id="marcas-page">
    <section class="sect2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                        <div class="filtros">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Marca</label>
                                        <select name="" id="" class="form-control">
                                            <option value="" >Marca 1</option>
                                            <option value="" >Marca 2</option>
                                            <option value="" >Marca 3</option>
                                            <option value="" >Marca 4</option>
                                            <option value="" >Marca 5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Ordenar por</label>
                                        <select name="" id="" class="form-control">
                                            <option value="" >Opcion 1</option>
                                            <option value="" >Opcion 2</option>
                                            <option value="" >Opcion 3</option>
                                            <option value="" >Opcion 4</option>
                                            <option value="" >Opcion 5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="vehiculos">

                        <div class="row">
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <a href="detalles.php" class="btn btn-info pull-left">Ver detalles</a>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <a href="detalles.php" class="btn btn-info pull-left">Ver detalles</a>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <a href="detalles.php" class="btn btn-info pull-left">Ver detalles</a>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <a href="detalles.php" class="btn btn-info pull-left">Ver detalles</a>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <a href="detalles.php" class="btn btn-info pull-left">Ver detalles</a>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 text-center">
                                <div class="box">
                                    <div class="img">
                                        <img class="img-responsive" src="./img/fotos/12.jpeg" alt="">
                                        <span class="estatus">Recién llegado</span>
                                    </div>
                                    <div class="info">
                                        <h4>Titulo</h4>
                                        <span class="precio pull-right">$12.990.00</span>
                                        <div class="specs">
                                            <div class="col-xs-4 border">
                                                2013
                                            </div>
                                            <div class="col-xs-4 border">
                                                Gasolina
                                            </div>
                                            <div class="col-xs-4">
                                                Automático
                                            </div>
                                            <div class="col-xs-12">
                                                <br>
                                                <span class="ion-model-s"></span> 54000 km
                                            </div>
                                        </div>
                                        <a href="detalles.php" class="btn btn-info pull-left">Ver detalles</a>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Cotizar ahora</button>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="row">
                                <div class="col-md-12">
                                        <div class="marcas">
                                            <div class="row">
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                                <div class="col-xs-3 col-sm-2">
                                                    <img class="img-responsive" src="img/marca1.png" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

</div>












<?php  include "modals.php"; include "partials/footer.php"; ?>